FROM busybox AS base

# with this line, nobody; without this line, root
USER nobody:nogroup

CMD ["/bin/id"]
